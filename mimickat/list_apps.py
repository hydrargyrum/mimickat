# SPDX-License-Identifier: WTFPL

from fnmatch import fnmatch

import yaml

from .apps import app_to_mime


def main(args):
	pattern = args.pattern or "*"

	print(yaml.dump({
		str(app_id): list(mimes)
		for app_id, mimes in app_to_mime.items()
		if fnmatch(app_id, pattern)
	}))
