# SPDX-License-Identifier: WTFPL

from fnmatch import fnmatch

import yaml

from .apps import mime_to_app


def main(args):
	print(yaml.dump({
		mime: [str(path) for path in app_paths]
		for mime, app_paths in mime_to_app.items()
		if fnmatch(mime, args.pattern)
	}))
