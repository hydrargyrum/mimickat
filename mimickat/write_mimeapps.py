# SPDX-License-Identifier: WTFPL

import configparser
from pathlib import Path

import yaml

from .apps import StrCase, parse_mimes, app_to_mime


def main(args):
	with open(args.conf) as fp:
		config = yaml.safe_load(fp)
	output = configparser.ConfigParser()

	addedmime = {}
	defaultmime = {}

	def add_default_app_for_mimes(app_id, mimes):
		app_id = StrCase(app_id)
		for mime in parse_mimes(app_id, mimes):
			defaultmime.setdefault(mime, []).append(app_id)
			if mime not in app_to_mime[app_id]:
				addedmime.setdefault(mime, []).append(app_id)


	for confkey, confvalues in config.items():
		if "/" in confkey:
			mime, app_ids = confkey, confvalues
			for app_id in app_ids:
				add_default_app_for_mimes(app_id, [mime])
		else:
			app_id, mimes = confkey, confvalues
			add_default_app_for_mimes(app_id, mimes)

	output["Added Associations"] = {
		mime: ";".join(apps)
		for mime, apps in addedmime.items()
	}
	output["Default Applications"] = {
		mime: ";".join(apps)
		for mime, apps in defaultmime.items()
	}

	output_path = Path.home() / ".config/mimeapps.list"
	with output_path.open("w") as fp:
		output.write(fp)
