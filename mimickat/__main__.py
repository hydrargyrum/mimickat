#!/usr/bin/env python3

from argparse import ArgumentParser

from . import apps_for_mime, list_apps, write_mimeapps


parser = ArgumentParser()
parser.set_defaults(module=None)
inter = parser.add_subparsers(required=True)

sub = inter.add_parser("apps-for-mime")
sub.set_defaults(module=apps_for_mime)
sub.add_argument("pattern")

sub = inter.add_parser("list-apps")
sub.set_defaults(module=list_apps)
sub.add_argument("pattern", nargs="?")

sub = inter.add_parser("write-defaults")
sub.set_defaults(module=write_mimeapps)
sub.add_argument("conf")

args = parser.parse_args()
args.module.main(args)
