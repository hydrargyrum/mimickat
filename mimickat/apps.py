# SPDX-License-Identifier: WTFPL

import configparser
import fnmatch
import functools
from pathlib import Path

import xdg.BaseDirectory
import xdg.DesktopEntry


@functools.total_ordering
class StrCase(str):
	# XXX functools.cache seems to work poorly with methods, so we can't
	# use @cache on `def lower(self)`
	@functools.cached_property
	def _low(self):
		return super().lower()

	def lower(self):
		return self._low

	def __hash__(self):
		return hash(self.lower())

	def __eq__(self, other):
		return self.lower() == other.lower()

	def __lt__(self, other):
		return self.lower() < other.lower()


def desktop_paths(app):
	return xdg.BaseDirectory.load_data_paths(f"applications/{app}")


def load_desktop(file):
	de = xdg.DesktopEntry.DesktopEntry(file)
	#de.parse(file)
	return de


def build_capabilities():
	app_to_mime = {}
	mime_to_app = {}
	for appdir in xdg.BaseDirectory.load_data_paths("applications"):
		for apppath in Path(appdir).glob("*.desktop"):
			de = load_desktop(str(apppath))
			app_id = StrCase(apppath.name)
			app_to_mime[app_id] = set(de.getMimeTypes())
			for mime in app_to_mime[app_id]:
				mime_to_app.setdefault(mime, set()).add(apppath)

	return app_to_mime, mime_to_app


def parse_mimes(app, mimes):
	#for mime in mimes.split(","):
	for mime in mimes:
		#mime = mime.strip()
		if "*" not in mime:
			yield mime
		else:
			yield from fnmatch.filter(app_to_mime[app], mime)


def parse_app_config(path):
	parser = configparser.ConfigParser()
	parser.read(path)
	return parser


def build_mimeapps():
	config = configparser.ConfigParser()
	return config


app_to_mime, mime_to_app = build_capabilities()
