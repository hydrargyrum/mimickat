# mimickat

Bulk configure what desktop apps should open which file types.

## The problem

Apps come with [`.desktop` files](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html), stating which MIME types they support, this is good.
But that alone doesn't tell which one is preferred when multiple apps open the same file type.
For example, [GIMP](https://www.gimp.org/) and [geeqie](https://www.geeqie.org/), can both open image files, but GIMP is an image editor and geeqie is an image viewer. For day to day file browsing, it's much more convenient to use geeqie.

It happened to me that for unknown reasons, the desktop deemed wine's notepad the most appropriate app for opening `text/plain` files, or handbrake for opening `audio/mpeg`.
Those apps can indeed open those files, but are highly inconvenient or inappropriate for merely viewing a file.

There's a [`mimeapps.list`](https://specifications.freedesktop.org/mime-apps-spec/latest/) config file for setting such preferences.
Graphical apps like `xfce4-mime-settings` (see screenshot below) can modify that config to set the preferred apps.

![xfce4-mime-settings](docs/xfce4-mime-settings.png)

However, having to set every image type, one by one, to the same app, is pretty tedious. Then repeat for every video type, etc.

**This is where mimickat fills the gap.**

It's possible to set an app for a whole category of MIME types, e.g. `image/*` (wildcard).

## Sample configuration

This configuration makes every image file opened with gpicview in priority and then with GIMP:

```
image/*:
    - gpicview.desktop
    - gimp.desktop
```

### More complex configuration

```
image/svg*:
    - org.inkscape.inkscape.desktop

image/gif*:
    - qgifview.desktop

image/*:
    - gpicview.desktop
    - gimp.desktop
```

Basically (more details below), this configuration will prefer inkscape for SVG files and gpicview then GIMP for any image.
It's not a problem if any app is not installed, there's always fallback.
For example:

- if inkscape is not installed, SVG files will be opened with gpicview or gimp, i.e. `image/*` includes `image/svg*`
- GIF files are opened preferably with [qgifview](https://gitlab.com/hydrargyrum/attic/-/tree/master/qgifview), but gpicview and gimp are used if it's not installed
- if some other image type is not handled by gpicview, gimp will be used as fallback, apps should be declared in order of preference
- if none of them are installed, it will fallback on whatever app supports opening images, i.e. the config is not strict

## Finding the MIME type

- `file -i SOME_FILE.EXT` is not always the correct command to find the MIME type of a particular file
- `xdg-mime query filetype SOME_FILE.EXT` will report a type closer to what will be used by (for example) a file manager to determine the correct command that should open a particular file

For example, for some `.opus` file:

- `file` command yields `audio/opus`
- `xdg-mime` command yields `audio/x-opus+ogg` (and that's what will be used in the configuration)
